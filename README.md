<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Tpfs Log Event Procmacro](#tpfs-log-event-procmacro)
  - [Faster Check of Source Code without Compiling](#faster-check-of-source-code-without-compiling)
  - [Running Build and Tests](#running-build-and-tests)
  - [Running Linting](#running-linting)
  - [Running Auditing](#running-auditing)
  - [Publishing of Prerelease Crates](#publishing-of-prerelease-crates)
  - [Publishing a version change to a crate](#publishing-a-version-change-to-a-crate)
  - [Understanding Semantic Versioning](#understanding-semantic-versioning)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Tpfs Log Event Procmacro

This crate is used to help implement the `tpfs-logger-port::LogKVPair` trait by converting enums in to the LogKVPair with
the use of an attribute macro.

## Faster Check of Source Code without Compiling

```shell
cargo check
```

## Running Build and Tests

```shell
cargo make build
cargo make test
```

## Running Linting

```shell
cargo make lint
```

## Running Auditing

```shell
cargo make audit
```

## Publishing of Prerelease Crates

Please see the versioning guidelines to understand prereleases: https://gitlab.com/TransparentIncDevelopment/docs/engineering-guide/blob/versioning-proposal/versioning.md

You should use prereleases to test out a concept in a different crate. However this is not intended for your final releases. See [Publishing changes to a crate](#publishing-changes-to-a-crate) for finalizing.

You can manually push a prerelease version for a branch by finding the corresponding pipeline in gitlab and clicking the play button next to the `manual-publish-prerelease-crate` job.
Prerelease crates will also be automatically published as merges occur to master.

## Publishing a version change to a crate

The versioning of the crate should be maintained via [semantic versioning](https://semver.org/) and there are helper scripts to help update the version in the Cargo.toml and Cargo.lock files. This version change can happen on your branch hopefully right before the MR is approved to be merged.

Use one of the following:
* `cargo make publish-patch`
* `cargo make publish-minor`
* `cargo make publish-major`

In order to actually publish the change this should be done after an MR has merged to master. Once merged pull from master and rebase then tag a release using the script helper.

It would look like the following:

```shell
git checkout master
git pull
cargo make tag-release
git push --tags
```

## Understanding Semantic Versioning
We have something written up in our engineering guidelines: https://gitlab.com/TransparentIncDevelopment/docs/engineering-guide/blob/versioning-proposal/versioning.md

The basics of semver is if the only changes included are fixes and adjustments then it's just a patch version number bump. Then if there are additions to functionality it's a minor version bump, and if it's a breaking change then it's a major number bump. The precendence is set at if there's a major version bump for a breaking change it would encompass any additional functionality without needed to bump the minor version. The same is true for minor version bumps that take priority over a patch version bump.

An example of that would be if the version number is `1.0.0` and there was additional functionality along with some fixes this would update the version to `1.1.0`.
