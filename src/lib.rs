#![forbid(unsafe_code)]
#![recursion_limit = "128"]

extern crate proc_macro;
extern crate serde_derive;

use quote::ToTokens;

use proc_macro::TokenStream;
use quote::quote;
use syn::{Data, DataEnum, DeriveInput, Fields};

/// This macro is used to break up an enum into structs of the same shape, auto-derive an implementation
///   of the `name` and `value` functions in the [tpfs_logger_port::LogKVPair] trait, and create a
///   mod that replaces the enum but with the same name--to be more reader-friendly.
#[proc_macro_attribute]
pub fn logging_event(_args: TokenStream, input: TokenStream) -> TokenStream {
    let ast: DeriveInput = syn::parse(input).unwrap();

    // Error out if we're not annotating an enum
    let data: DataEnum = match ast.data {
        Data::Enum(d) => d,
        _ => panic!("LoggingEvent can only be derived for enums"),
    };

    let name = ast.ident;

    let variant_tokens_streams = data.variants.iter().map(|v| {
        let var_id = &v.ident;
        let stringified = var_id.to_string();

        let fields = v.fields.clone().into_token_stream();
        // Convert the enum variant into a struct with the same shape.
        let struct_tokens = match &v.fields {
            Fields::Unnamed(fields) => {
                let fields = fields.unnamed.iter().map(|f| {
                    let f_ty = &f.ty;
                    quote! {
                         pub #f_ty
                    }
                });
                quote! {
                    #[derive(::serde_derive::Serialize, Clone, Debug)]
                    pub struct #var_id(#(#fields),*);
                }
            }
            Fields::Named(fields) => {
                let fields = fields.named.iter().map(|f| {
                    let f_id = f.ident.as_ref().unwrap();
                    let f_ty = &f.ty;
                    quote! {
                         pub #f_id: #f_ty
                    }
                });
                quote! {
                    #[derive(::serde_derive::Serialize, Clone, Debug)]
                    pub struct #var_id {
                        #(#fields),*
                    }
                }
            }
            _ => {
                quote! {
                    #[derive(::serde_derive::Serialize, Clone, Debug)]
                    pub struct #var_id #fields;
                }
            }
        };
        // Implement the LogKVPairName and LogKVPairValue for the new struct.
        let impl_tokens = quote! {
            impl ::tpfs_logger_port::LogKVPairName for #var_id {
                fn name(&self) -> &'static str {
                    #stringified
                }
            }

            impl ::tpfs_logger_port::LogKVPairValue for #var_id {
                type T = Self;

                fn value(&self) -> Self::T {
                    self.clone() // TODO: We can probably get rid of this clone()
                }
            }
        };
        quote! {
            #struct_tokens
            #impl_tokens
        }
    });

    // Gather all the token streams into new module with a name matching the original enum
    let gen = quote! {
        #[allow(non_snake_case)]
        pub mod #name {
            use super::*;
            #(#variant_tokens_streams)*
        }
    };
    gen.into()
}
